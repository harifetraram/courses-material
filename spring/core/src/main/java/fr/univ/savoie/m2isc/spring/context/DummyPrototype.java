package fr.univ.savoie.m2isc.spring.context;

public class DummyPrototype {

   private String message;

   public void setMessage(String message){
      this.message  = message;
   }

   public void getMessage(){
      System.out.println("Your Message : " + message);
   }
}