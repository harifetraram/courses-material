package fr.univ.savoie.m2isc.spring.context;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {


    public static void main(String[] args) {

        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("application-context.xml");

        DummySingleton dummySingleton1 = ctx.getBean("dummySingleton", DummySingleton.class);
        DummySingleton dummySingleton2 = ctx.getBean(DummySingleton.class);

        System.out.println(dummySingleton1);
        System.out.println(dummySingleton2);
        System.out.println();

        DummyPrototype dummyPrototype1 = ctx.getBean(DummyPrototype.class);
        DummyPrototype dummyPrototype2 = ctx.getBean(DummyPrototype.class);

        System.out.println(dummyPrototype1);
        System.out.println(dummyPrototype2);


    }

}
