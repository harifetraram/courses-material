<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>hello world</title>
</head>
<body>
<h1>hello spring mvc world</h1>

<form:form method="post" action="${pageContext.request.contextPath}/create.do ">
    <label>Dummy</label> : <input type="text" name="dummyInput"/>
    <input type="submit"/>
</form:form>

</body>
</html>
