package fr.univ.savoie.m2isc.springorm.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class LogEntry implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String payload;

    private String creator;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timeStamp;

    public Date getTimeStamp() {
        return timeStamp;
    }

    @PrePersist
    @PreUpdate
    void onSave() {
        timeStamp = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String entry) {
        this.payload = entry;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public LogEntry payload(final String payload) {
        this.payload = payload;
        return this;
    }

    public LogEntry creator(final String creator) {
        this.creator = creator;
        return this;
    }


}