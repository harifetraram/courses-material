package fr.univ.savoie.m2isc.springcore;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.inject.Inject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class SpringCoreDemoServlet extends HttpServlet {

    @Inject
    private DummyBean dummyBean;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Welcome to good old time... back in 1999</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1> L'injection marche a condition de faire appel explicitement au moteur SpringBeanAutowiringSupport</h1>");
        out.println("<h1> Injected bean : '" + dummyBean + "'</h1>");
        out.println("</body>");
        out.println("</html>");

    }
}
