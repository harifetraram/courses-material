package fr.univ.savoie.m2isc.springcore;

import javax.inject.Named;
import javax.inject.Singleton;

@Named
@Singleton
public class DummyBean {

    @Override
    public String toString() {
        return "fr.univ.savoie.m2isc.springcore.DummyBean : I'have been instanciated by Springframework";
    }

}
