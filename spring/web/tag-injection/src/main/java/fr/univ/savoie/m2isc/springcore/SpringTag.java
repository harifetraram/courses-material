package fr.univ.savoie.m2isc.springcore;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.inject.Inject;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class SpringTag extends TagSupport {

    @Inject
    private DummyBean dummyBean;

    @Override
    public void setPageContext(PageContext pageContext) {
        super.setPageContext(pageContext);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, pageContext.getServletContext());
    }

    @Override
    public int doStartTag() throws JspException {

        try {
            pageContext.getOut().print(dummyBean);
        } catch (IOException e) {
            throw new JspException();
        }

        return SKIP_BODY;

    }


}
